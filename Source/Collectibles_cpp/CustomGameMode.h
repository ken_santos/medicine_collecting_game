// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "GameFramework/Actor.h"
#include "CustomGameMode.generated.h"

/**
 * 
 */
UCLASS()
class COLLECTIBLES_CPP_API ACustomGameMode : public AGameMode
{
	GENERATED_BODY()
public:

	ACustomGameMode();

	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		TSubclassOf<APawn> PlayerRecharge;

	float SpawnZ = 500.0f;

	UPROPERTY(EditAnywhere)
		float SpawnMinX;

	UPROPERTY(EditAnywhere)
		float SpawnMaxX;

	UPROPERTY(EditAnywhere)
		float SpawnMinY;

	UPROPERTY(EditAnywhere)
		float SpawnMaxY;

	void SpawnPickup();

};
