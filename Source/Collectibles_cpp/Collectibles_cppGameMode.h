// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Collectibles_cppGameMode.generated.h"

UCLASS(minimalapi)
class ACollectibles_cppGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACollectibles_cppGameMode();
};



