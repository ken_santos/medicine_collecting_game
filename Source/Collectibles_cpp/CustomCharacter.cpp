// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomCharacter.h"

// Sets default values
ACustomCharacter::ACustomCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCapsuleComponent()->InitCapsuleSize(42.0f, 96.0f);

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f);
	GetCharacterMovement()->JumpZVelocity = 600.0f;
	GetCharacterMovement()->AirControl = 0.2f;

	CameraSpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraSpringArm"));
	CameraSpringArm->SetupAttachment(RootComponent);
	CameraSpringArm->TargetArmLength = 300.0f;
	CameraSpringArm->bUsePawnControlRotation = true;

	ThirdPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ThirdPersonCamera"));
	ThirdPersonCamera->SetupAttachment(CameraSpringArm, USpringArmComponent::SocketName);
	ThirdPersonCamera->bUsePawnControlRotation = false;

	bDead = false;

	Health = 100.0f;
}

// Called when the game starts or when spawned
void ACustomCharacter::BeginPlay()
{
	Super::BeginPlay();
	GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ACustomCharacter::OnBeginOverlap);

	if (PlayerHealthWidgetClass != nullptr) {
		PlayerHealthWidget = CreateWidget(GetWorld(), PlayerHealthWidgetClass);
		PlayerHealthWidget->AddToViewport();
	}
}

// Called every frame
void ACustomCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Health -= DeltaTime * HealthThreshold;

	if (Health <= 0.0f) {
		if (!bDead) {
			bDead = true;

			GetMesh()->SetSimulatePhysics(true);

			FTimerHandle unusedHandle;
			GetWorldTimerManager().SetTimer(
			unusedHandle, this, &ACustomCharacter::RestartCurrentLevel, 3.0f, false);

		}
	}
}

// Called to bind functionality to input
void ACustomCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &ACustomCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACustomCharacter::MoveRight);
}

void ACustomCharacter::MoveForward(float Axis)
{
	if (!bDead) {
		const FRotator controlRotation = Controller->GetControlRotation();
		const FRotator yawRotation = FRotator(0.0f, controlRotation.Yaw, 0.0f);

		const FVector movementDirection = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(movementDirection, Axis);
	}
}

void ACustomCharacter::MoveRight(float Axis)
{
	if (!bDead) {
		const FRotator controlRotation = Controller->GetControlRotation();
		const FRotator yawRotation = FRotator(0.0f, controlRotation.Yaw, 0.0f);

		const FVector movementDirection = FRotationMatrix(yawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(movementDirection, Axis);
	}
}

void ACustomCharacter::RestartCurrentLevel()
{
	UGameplayStatics::OpenLevel(this, FName(GetWorld()->GetName()), false);
}

void ACustomCharacter::OnBeginOverlap(class UPrimitiveComponent* p_hitComp, class AActor* p_otherActor, class UPrimitiveComponent* p_otherComponent, int32 p_otherBodyIndex, bool bFromSweep, const FHitResult& p_hitResult)
{
	if (p_otherActor->ActorHasTag("Recharge")) {
		Health += 10.0f;
		if (Health >= 100.0f)
			Health = 100.0f;

		p_otherActor->Destroy();
	}
}

