// Copyright Epic Games, Inc. All Rights Reserved.

#include "Collectibles_cppGameMode.h"
#include "Collectibles_cppCharacter.h"
#include "UObject/ConstructorHelpers.h"

ACollectibles_cppGameMode::ACollectibles_cppGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
