// Fill out your copyright notice in the Description page of Project Settings.


#include "CustomGameMode.h"


ACustomGameMode::ACustomGameMode()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ACustomGameMode::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle unusedTimer;
	GetWorldTimerManager().SetTimer(
		unusedTimer, this, &ACustomGameMode::SpawnPickup, 
		FMath::RandRange(2.0f, 5.0f), true);
}

void ACustomGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ACustomGameMode::SpawnPickup()
{
	float randX = FMath::RandRange(SpawnMinX, SpawnMaxX);
	float randY = FMath::RandRange(SpawnMinY, SpawnMaxY);

	FVector spawnPosition = FVector(randX, randY, SpawnZ);
	FRotator spawnRotation = FRotator(0.0f, 0.0f, 0.0f);

	GetWorld()->SpawnActor(PlayerRecharge, &spawnPosition, &spawnRotation);
}